﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentMonkeys_MT
{
    public partial class Person
    {

        #region CRUD

        public long Save()
        {

            using (RMEntities context = new RMEntities())
            {

                if (this.Id > 0)
                {
                    ObjectSet<Person> objectSet = context.People;
                    objectSet.Attach(this);
                    objectSet.Context.ObjectStateManager.ChangeObjectState(this, System.Data.EntityState.Modified);

                }
                else
                {
                    context.People.AddObject(this);
                }

                context.SaveChanges();
                return this.Id;

            }

        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------

        public Person Load(long Id)
        {

            using (RMEntities context = new RMEntities())
            {
                Person person = context.People.FirstOrDefault(Person => Person.Id == Id);
                return person;
            }

        }

        #endregion

    }//class

}//namespace
