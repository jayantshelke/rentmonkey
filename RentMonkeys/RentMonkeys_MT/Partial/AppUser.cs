﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentMonkeys_MT
{
    public partial class AppUser
    {
        #region CRUD

        public long Save()
        {

            using (RMEntities context = new RMEntities())
            {

                if (this.Id > 0)
                {
                    ObjectSet<AppUser> objectSet = context.AppUsers;
                    objectSet.Attach(this);
                    objectSet.Context.ObjectStateManager.ChangeObjectState(this, System.Data.EntityState.Modified);

                }
                else
                {
                    context.AppUsers.AddObject(this);
                }

                context.SaveChanges();
                return this.Id;

            }

        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------

        public AppUser Load(long Id)
        {

            using (RMEntities context = new RMEntities())
            {
                AppUser appUser = context.AppUsers.FirstOrDefault(AppUser => AppUser.Id == Id);
                return appUser;
            }

        }

        #endregion

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------

        #region Service Methods

        public Boolean CheckUNEixst(string UN)
        {

            using (RMEntities context = new RMEntities())
            {
                Boolean IsExist = context.AppUsers.Where(AppUser => AppUser.IsActive && AppUser.UN == UN).Any();
                return IsExist;
            }

        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------

        public AppUser CheckUserCredentials(string UN, string Pwd)
        {

            using (RMEntities context = new RMEntities())
            {
                AppUser appUser = context.AppUsers.FirstOrDefault(AppUser => AppUser.IsActive && AppUser.UN == UN && AppUser.Pwd == Pwd);
                return appUser;
            }

        }

        #endregion

    }//class

}//namespace
