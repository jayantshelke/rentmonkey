﻿(function (factory) {
    if (typeof define === "function" && define.amd) {
        define(["jquery"], factory);
    } else {
        factory(jQuery);
    }
}(function ($) {

    $.extend($.fn, {

        validateform: function (data) {


            $.each(this[0], function (index, val) {

                if (val.tagName.toLowerCase() == "input" && val.type.toLowerCase() == "text") {

                    var ControlNm = $("#" + val.id);
                    var ValidationTypes = ControlNm.attr("validation");

                    if (ValidationTypes != null) {

                        //var ValidationTypes = val.attr("validation");
                        var ValidationType = ValidationTypes.split(" ");
                        var Message = "";

                        $.each(ValidationType, function (vIndex, vVal) {

                            vVal = vVal.toLowerCase();

                            switch (vVal) {

                                case "email":
                                    if (Message == "") {
                                        //var pattern = /^[a-z0-9!#$%&’'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&’'*+/=?^_`{|}~-]+)*@@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i;
                                        var pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                                        var strEmail = $.trim(ControlNm.val());

                                        if (pattern.test(strEmail) == false) {
                                            Message = "Invalid email."
                                        }

                                    }//if (Message != "") 

                                    break;
                                case "required":

                                    if (Message == "") {

                                        if (ControlNm.val() == "") {
                                            Message = "This field is required."
                                        }

                                    }//if (Message != "") 

                                    break;

                            }//switch (vVal) 

                        });

                        if (Message != "") {
                            $("#" + val.id).addClass("error");
                            $("#" + val.id + "-error").show();
                            $("#" + val.id + "-error").text(Message);
                            event.preventDefault();
                        }

                    }//if (ValidationTypes != null) 

                }//if (val.tagName.toLowerCase() == "input" && val.type.toLowerCase() == "text") 

            });

        }//validateform: function (data) 

    });

}));