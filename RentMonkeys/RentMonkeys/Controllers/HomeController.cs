﻿using RentMonkeys_MT;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

//FaceBook API References
using FacebookAPI;
using FacebookAPI.Managers;
using FacebookAPI.Objects;
using FacebookAPI.FacebookProperties;


namespace RentMonkeys.Controllers
{
    public class HomeController : Controller
    {
        #region LogIn

        public ActionResult LogIn()
        {
            return View();
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------

        [HttpPost]
        public ActionResult LogIn(FormCollection form)
        {

            try
            {

                if (form["btnLogIn"] != null)
                {
                    AppUser appUser = new AppUser();
                    string UN = form["txtUserName"];
                    string PWD = form["txtPassword"];
                    appUser = appUser.CheckUserCredentials(UN, PWD);

                    if (appUser != null)
                    {
                        Session["AppUserId"] = appUser.Id;
                        return RedirectToAction("Index", "Dashboard");
                    }

                }

                if (form["btnSignUp"] != null)
                {

                    string UN = form["txtUserName"];
                    string FirstName = form["txtFirstName"];
                    string LastName = form["txtLastName"];

                    Person person = Person.CreatePerson(0, FirstName, LastName, false, true, DateTime.Now, DateTime.Now);
                    long PersonSaveId = person.Save();

                    if (PersonSaveId > 0)
                    {
                        AppUser appUser = AppUser.CreateAppUser(0, UN, "", PersonSaveId, false, DateTime.Now, DateTime.Now);
                        long AppUserSaveId = appUser.Save();
                    }

                }


                if (form["btnGoogle"] != null)
                {

                }

                if (form["btnFacebook"] != null)
                {

                }

                if (form["btnLinkedIn"] != null)
                {

                }

            }
            catch (Exception)
            {

            }

            return View();
        }

        #endregion

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------

        #region Check Email Exist or Not

        public JsonResult CheckEmailExist(string Email)
        {

            bool IsExist = true;

            try
            {
                //get all user name from system and check current email is exist or not in the list
                AppUser appUser = new AppUser();
                IsExist = appUser.CheckUNEixst(Email);

            }
            catch (Exception Ex)
            {
                //if exception occurs dont allow to proceed
                IsExist = true;
            }

            return Json(IsExist, JsonRequestBehavior.AllowGet);

        }

        #endregion

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------

        #region Code For FB Login

        #region Commented Code
        //public ActionResult FBCall()
        //{

        //    var settings = ConfigurationManager.GetSection("facebookSettings");
        //    MySettings current;
        //    string apiKey;
        //    if (settings != null)
        //    {
        //        current = settings as MySettings;
        //        apiKey = current.ApiKey;
        //    }

        //    // ViewData["ApiKey"] = ConfigurationManager.AppSettings["FBAppSecret"];

        //    string code = Request.QueryString["code"];
        //    string appId = WebConfigurationManager.AppSettings["FBAppID"];
        //    string appSecret = WebConfigurationManager.AppSettings["FBAppSecret"];

        //    if (code == "" || code == null)
        //    {
        //        //request for authentication
        //        Response.Redirect("https://graph.facebook.com/oauth/authorize?client_id=" + appId + "&redirect_uri=http://localhost:5176/");
        //    }
        //    else
        //    {
        //        fb = new MyFB();
        //        fb.ApplicationSecret = appSecret;
        //        fb.ApplicationID = appId;
        //        string accessToken = fb.GetAccessToken(code);
        //        fb.AccessToken = accessToken;

        //        ViewData["MyName"] = fb.GetMyName();
        //    }

        //} 
        #endregion

        public void GetToken()
        {
            FacebookUserManager FUM = new FacebookUserManager();
            FacebookApplication fa = new FacebookApplication();
            FacebookApplicationManager fam = new FacebookApplicationManager();
            string AccessToken = fam.GetAppAccessToken(CGlobalUI.AppId, CGlobalUI.AppSecret);
            StatusMessageManager smm = new StatusMessageManager();
            

 
        }

        #endregion

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------

    }//class

}//namespace